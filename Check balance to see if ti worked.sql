 select a.year_run, a.activity, a.moru, a.put_name, a.make_use_base_amount, a.makeuse_amount, 
  case when a.moru = 'U' and a.activity not like 'Export Consumer%' then
	a.makeuse_amount * (1-b.factor) else a.makeuse_amount end as scaled_amount
  from tech_opt.all_4_amounts a
  join tech_opt.use_down_i_up_factor b
     on a.year_run = b.year_run
     and a.put_name = b.put_name
  where a.put_name like 'S598%';