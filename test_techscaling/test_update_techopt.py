import csv
import unittest
from itertools import chain
from os.path import join

from hbautil import scriptutil as su, pecassetup
from pecas_test import mocksettings, mockscen, filetest
from techscaling import update_techopt as uto


class TestTechnologyScaling(unittest.TestCase):
    def setUp(self):
        self.scendir = join("test", "techscaling")
        self.ps = mocksettings.PecasSettings().with_machine_settings().with_test_sd().between_years(
            2009, 2010
        ).with_scenario_directory(
            self.scendir
        ).clone_with(
            importer_string="%Importers",
            exporter_string="%Exporters",
        )
        mockscen.create_database(self.ps)
        pecassetup.sd_querier(self.ps).query("create schema tech_opt")

    def testTechnologyScaling(self):
        # class Settings(object):
        #     def __init__(self):
        #         self.input_dir = "technology_scaling_test/parameters"
        #         self.script_dir = "technology_scaling_scripts"
        #         self.working_dir = "technology_scaling_test"
        #         self.cur_working_dir = "technology_scaling_test/t46"
        #
        #         self.baseyear = 19
        #         self.base_working_dir = "technology_scaling_test/t19"
        #         self.curyear = 46
        #
        #         self.importer_string = "%Importers"
        #         self.exporter_string = "%Exporters"

        uto.create_all_acttot_techopt(self.ps, lambda: pecassetup.connect_to_sd(self.ps))

        expected_acttot = self.read_acttot(join(self.scendir, "2010", "ExpectedActivityTotalsI.csv"))
        actual_acttot = self.read_acttot(join(self.scendir, "2010", "ActivityTotalsI.csv"))
        self.assertActivityTotalsEqual(expected_acttot, actual_acttot)

        expected_techopt = self.read_techopt(join(self.scendir, "2010", "ExpectedTechnologyOptionsI.csv"))
        actual_techopt = self.read_techopt(join(self.scendir, "2010", "TechnologyOptionsI.csv"))
        self.assertTechnologyOptionsEqual(expected_techopt, actual_techopt)

    def assertActivityTotalsEqual(self, expected, actual, abs_tolerance=1e-7, rel_tolerance=1e-7):
        for act in chain(expected.keys(), actual.keys()):
            self.assertIn(act, expected, "Activity {!r} is missing from expected".format(act))
            self.assertIn(act, actual, "Activity {!r} is missing from actual".format(act))

            expected_amt, actual_amt = expected[act], actual[act]
            self.assertCloseEnough(
                expected_amt, actual_amt, abs_tolerance, rel_tolerance,
                "activity {!r}".format(act)
            )

    def assertTechnologyOptionsEqual(self, expected, actual, abs_tolerance=1e-7, rel_tolerance=1e-7):
        for key in chain(expected.keys(), actual.keys()):
            act, opt = key
            self.assertIn(key, expected, "Option {!r} of activity {!r} is missing from expected".format(opt, act))
            self.assertIn(key, actual, "Option {!r} of activity {!r} is missing from actual".format(opt, act))

            expected_coeffs, actual_coeffs = expected[key], actual[key]
            for put in chain(expected_coeffs, actual_coeffs):
                self.assertIn(put, expected_coeffs, "Activity {!r} is missing from expected".format(put))
                self.assertIn(put, actual_coeffs, "Activity {!r} is missing from actual".format(put))

                expected_coeff, actual_coeff = expected_coeffs[put], actual_coeffs[put]
                self.assertCloseEnough(
                    expected_coeff, actual_coeff, abs_tolerance, rel_tolerance,
                    "activity {!r}, option {!r}, put {!r}".format(act, opt, put)
                )

    def assertCloseEnough(self, expected, actual, abs_tolerance, rel_tolerance, msg):
        tolerance = max(rel_tolerance * abs(expected + actual), abs_tolerance)
        self.assertAlmostEqual(
            expected, actual, delta=tolerance, msg="Values not equal for {} ({} vs {})".format(msg, expected, actual))

    @staticmethod
    def read_acttot(fname):
        result = {}
        with open(fname, "r") as inf:
            reader = csv.reader(inf)
            header = next(reader)
            act_col = header.index("Activity")
            amt_col = header.index("TotalAmount")
            for line in reader:
                result[line[act_col]] = float(line[amt_col])
        return result

    @staticmethod
    def read_techopt(fname):
        result = {}
        with open(fname, "r") as inf:
            reader = csv.reader(inf)
            header = next(reader)
            act_col = header.index("Activity")
            opt_col = header.index("OptionName")
            wt_col = header.index("OptionWeight")
            for line in reader:
                result[line[act_col], line[opt_col]] = {k: float(v) for k, v in zip(header[wt_col:], line[wt_col:])}
        return result


class TestTechnologySubstitution(filetest.FileTestCase):
    def setUp(self):
        self.scendir = join("test", "technology-substitution")
        self.ps = mocksettings.PecasSettings().between_years(2011, 2080).with_scenario_directory(
            self.scendir
        )

    def test_substitute_technology_columns(self):
        for year in su.irange(2020, 2026):
            uto.substitute_technology_columns(self.ps, year, 2011)
            self.assertFilesEqual(
                join(self.scendir, str(year), "TechnologyOptionsI.csv"),
                join(self.scendir, str(year), "TechnologyOptionsIExpected.csv"),
            )

    def test_substitute_technology_columns_no_refyear(self):
        for year in su.irange(2020, 2026):
            uto.substitute_technology_columns(self.ps, year)
            self.assertFilesEqual(
                join(self.scendir, str(year), "TechnologyOptionsI.csv"),
                join(self.scendir, str(year), "TechnologyOptionsIExpected.csv"),
            )

    def test_dont_substitute_technology_columns(self):
        # If there aren't any substitution files since the ref year,
        # the TechnologyOptionsI file should just be a copy of the
        # TechnologyOptionsScaled file.
        uto.substitute_technology_columns(self.ps, 2026, 2026)
        self.assertFilesEqual(
            join(self.scendir, "2026", "TechnologyOptionsI.csv"),
            join(self.scendir, "2026", "TechnologyOptionsScaled.csv"),
        )
