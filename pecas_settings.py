#Scenario-specific settings for PECAS.

# Re-export all machine-specific settings so they can be referenced through this module.
from machine_settings import *

from pecas_routines import irange
from os.path import join

# Whether to reset the SD  database before running
reset_initial_database = True
load_output_to_mapit = True

baseyear=2011
# First year in which AA is run. If later than the base year, existing AA
# results are used until the start year.
aa_startyear=2011
# for the time being we are running AA every 2 years (odd numbered years)
aayears=range(2011,2050,2)
# First year in which TM is run. If later than the baseyear, existing skims are
# used until the start year.
tm_startyear=2051
# First year in which SD is run instead of being replayed. SD is replayed from
# baseyear until the start year
sd_startyear=2013
# Set to true to resume from where the model stopped or crashed.
resume_run = False
# Last year in which AA is run (SD stops one year before this)
endyear=2013
# Years in which new skims are available
skimyears=[2011]
# Years in which AA should run constrained
constrainedyears=[2011]
# Years in which the travel model should run
tmyears=skimyears[1:]
# Years in which the SD parcel database should be snapshotted
# This can be replaced with arbitrary lists of years, for example:
# snapshotyears=[2018, 2022, 2026]
snapshotyears=irange(2011,2037)+[2045,2050]
# Set to false if not using standard SD
standard_sd = True

sd_schema = 'i61a'
scenario = 'I61a'

# Uncomment the following if the PECAS run script needs to run a travel model.
# use_tm = True

# Pattern for activities that are importers and those that are exporters.
importer_string = "Import Provider%"
exporter_string = "Export Consumer%"
# Pattern for skim files; any copies of "{yr}" will be replaced by the skim year
skim_fname = "ATPecasSkims"
# Column in the skims file where travel distances are stored.
distance_column = "distance"

codepath = join(scendir, "AllYears", "Code")
inputpath = join(scendir, "AllYears", "Inputs")

gravity_exponent = -2.0

# Calculate expected amounts of development in SD
find_expected_values_years = []
expected_value_parcels = "parcels"
expected_value_targets = "targets_5pct.csv"

# Set these to True to turn them on in the run script, False to turn them off.

# There is also a version of price smoothing available within SD. To turn that on,
# ensure that apply_price_smoothing_outside_of_sd is set to False, then turn on
# SmoothPrices in sd.properties.
apply_price_smoothing_outside_of_sd = False
calculate_import_export_size = True
floorspace_calc_delta = True
load_tm_totals = False
allocate_tm_totals = False # Python/SQL creation of travel model inputs
allocate_am_totals = False # ABM inputs (Atlanta)
load_abm_land_use = False # ABM land use (Atlanta)
generate_cityphi = False # Create the CityPhi crosstab table

use_aa_to_sd_price_ratio = True

labour_make_use = False
apply_sitespec = False

update_space_limits = False

# PECAS jar file names
pecasjar="PecasV2.9_r5821.jar"
commonbasejar="common-base_r4640.jar"
simpleormjar="simple_orm_r2580.jar"

# Java memory settings
aa_memory="48000M"
sd_memory="48000M"

# Other Java settings
profiling=False
missionControl=True

# FloorspaceI column names
flItaz="TAZ"
flIcommodity="Commodity"
flIquantity="Quantity"
long_query_test_log="tm_queries.log"

# ADDED BY MRS-GUI
# Please don't change unless you are NOT running this from the GUI
#  (or otherwise know what you are doing)
run_from_mrsgui = True
