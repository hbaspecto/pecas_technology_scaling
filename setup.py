from setuptools import setup

setup(
    name="techscaling",
    version="1.0",
    packages=["techscaling"],
    package_data={
        "": [
            "*.sh",
            "*.sql",
            ],
    },
    install_requires=[
        "setuptools-git",
    ],
    entry_points={
        "console_scripts": [
            "update_techopt = techscaling.update_techopt:main",
        ]
    }
)
