set search_path = tech_opt;

drop view if exists tech_opt.aef_ev cascade;
create view tech_opt.aef_ev
as select year_run, am.activity, moru,
topt.put_name,
am.total_amount * topt.coefficient as amount
from tech_opt.activity_totals am
join tech_opt.technology_options topt on am.activity = topt.activity
where topt.option_name = 'EV';

drop view if exists tech_opt.aef_weighted cascade;
create view tech_opt.aef_weighted
as select year_run, am.activity, moru, 
topt.put_name,
am.total_amount * topt.coefficient as amount
from tech_opt.activity_totals am
join 
   (select i.activity, i.put_name, i.moru, sum(i.option_weight*i.coefficient/allopts.sum_weight) as coefficient
     from tech_opt.technology_options i
     join (select activity, sum(weight) as sum_weight from tech_opt.tech_option group by activity) allopts
     on i.activity = allopts.activity group by i.activity, i.put_name, i.moru) 
topt on am.activity = topt.activity;
     
drop view if exists tech_opt.aef_make_use cascade;
create view tech_opt.aef_make_use
as select year_run, am.activity, moru,
topt.put_name,
am.total_amount * topt.coefficient as amount,
topt.amount as make_use_base_amount
from tech_opt.activity_totals am
join tech_opt.make_use topt on am.activity = topt.activity;

drop view if exists tech_opt.all_3_amounts;
drop view if exists tech_opt.all_4_amounts;
create view tech_opt.all_4_amounts
as select b.year_run, b.activity, b.moru, b.put_name, 
a.amount as ev_amount, b.amount as weighted_amount, c.amount as makeuse_amount, c.make_use_base_amount
from tech_opt.aef_weighted b
left join tech_opt.aef_ev a on 
	a.activity = b.activity and a.put_name = b.put_name and a.moru = b.moru and a.year_run = b.year_run
left join tech_opt.aef_make_use c on
	c.activity = b.activity and c.put_name = b.put_name and c.moru = b.moru and c.year_run = b.year_run;



