set search_path = tech_opt;

select null as "using constraints to check names of activities";

ALTER TABLE tech_opt.activity_totals
   ADD CONSTRAINT activity_total_activity
   FOREIGN KEY (activity) 
   REFERENCES tech_opt.activity(activity);