        select year_run as "YearRun",
        activity as "Activity",
        moru as "Moru",
        put_name as "PutName",
        ev_amount as "EvAmount",
        weighted_amount as "WeightedAmount",
        makeuse_amount as "MakeUseAmount",
        make_use_base_amount as "MakeUseBaseAmount"
        from tech_opt.all_4_amounts