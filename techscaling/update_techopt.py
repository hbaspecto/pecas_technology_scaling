import argparse
import csv
import logging
import os
import shutil
from os.path import join, basename

import pecas_routines as pr
from hbautil.scriptutil import irange, in_this_directory, colindices, elements
from hbautil.sqlutil import Querier


def create_all_acttot_techopt(
        ps, connect, acttot_src=None, first_year=None, end_year=None, ref_year=None,
        output_file='TechnologyOptionsI.csv'):
    if first_year is None:
        first_year = ps.baseyear + 1
    if end_year is None:
        end_year = ps.endyear
    tr = prepare_scripts(ps, connect, reference_year=ref_year)
    #print("Output file is "+str(output_file))
    with tr:
        load_technology_options(ps, tr, tech_opt_year=ref_year)

        tr.query_external(_script_dir("002_load_tech.sql"))

        load_all_activity_totals(ps, tr, acttot_src)
        load_make_use(ps, tr, mu_year=ref_year)

        calculate_scaling(ps, tr)

        for year in irange(first_year, end_year):
            update_techopt(ps, tr, year, ref_year=ref_year, output_file=output_file)

            tr.dump_to_csv(
                'select activity as "Activity",\n'
                'total_amount as "TotalAmount"\n'
                'from tech_opt.activity_totals\n'
                'where year_run = %(year)s',
                join(ps.scendir, str(year), "ActivityTotalsI.csv"),
                year=year
            )


def _script_dir(fname):
    return in_this_directory(__file__, fname)


def prepare_scripts(ps, connect, reference_year=None):
    if reference_year is None:
        reference_year = ps.baseyear
        os.environ["TECHDIR"] = join(ps.scendir, "AllYears", "Inputs")
    else:
        os.environ["TECHDIR"] = join(ps.scendir, str(reference_year))
    os.environ["ACTDIR"] = join(ps.scendir, "AllYears", "Inputs")
    os.environ["MUDIR"] = join(ps.scendir, str(reference_year))
    pr.run_shell_script(ps, _script_dir("001_prepare_for_import.sh"))

    querier = Querier(connect, debug_log=Logger())

    args = dict(
        baseyear=ps.baseyear,
        importer=ps.importer_string.replace("%", "%%"),
        exporter=ps.exporter_string.replace("%", "%%"),
    )

    return querier.transaction(named_tuples=True, **args)


def calculate_scaling(ps, tr):
    tr.query_external(_script_dir("005_check_names.sql"))
    tr.query_external(_script_dir("010_aef_view.sql"))
    dump_all_4_amounts(ps, tr)
    tr.query_external(_script_dir("020_scale_exporters.sql"))
    tr.query_external(_script_dir("021_scale_importers.sql"))
    tr.query_external(_script_dir("022_calculate_use_scale.sql"))
    dump_use_down_i_up_factor(ps, tr)


def dump_all_4_amounts(ps, tr):
    tr.dump_to_csv(
        'select year_run as "YearRun",\n'
        'activity as "Activity",\n'
        'moru as "Moru",\n'
        'put_name as "PutName",\n'
        'ev_amount as "EvAmount",\n'
        'weighted_amount as "WeightedAmount",\n'
        'makeuse_amount as "MakeUseAmount",\n'
        'make_use_base_amount as "MakeUseBaseAmount"\n'
        'from tech_opt.all_4_amounts',
        join(ps.scendir, "AllYears", "Outputs", "TechOptAll4Amounts.csv")
    )


def dump_use_down_i_up_factor(ps, tr):
    tr.dump_to_csv(
        'select year_run as "YearRun",\n'
        'put_name as "PutName",\n'
        'net_exog_supply as "NetExogSupply",\n'
        'make_future_amount as "MakeFutureAmount",\n'
        'total_use_amount as "TotalUseAmount",\n'
        'import_amount as "ImportAmount",\n'
        'internaluse_amount as "InternalUseAmount",\n'
        'factor as "Factor"\n'
        'from tech_opt.use_down_i_up_factor',
        join(ps.scendir, "AllYears", "Outputs", "UseDownIUpFactor.csv")
    )


def load_technology_options(ps, tr, tech_opt_year=None):
    if tech_opt_year is None:
        path = join(ps.scendir, "AllYears", "Inputs")
    else:
        path = join(ps.scendir, str(tech_opt_year))
    tr.query("set search_path = tech_opt;")
    tr.query("drop table if exists tech_opt.technology_options cascade;")

    logging.info("Creating technology options table")
    tr.query(
        "create table tech_opt.technology_options\n"
        "(activity character varying,\n"
        "option_name character varying,\n"
        "option_weight double precision,\n"
        "put_name_code character varying,\n"
        "coefficient double precision);"
    )

    logging.info("Importing long thin tech options table")
    tr.load_from_csv(
        "tech_opt.technology_options",
        join(path, "TechOptLongThin.csv"),
        header=False
    )


def load_all_activity_totals(ps, tr, acttot_src):
    tr.query("set search_path = tech_opt;")

    if acttot_src is None:
        acttot_src = join(ps.scendir, ps.inputpath, "All_ActivityTotalsI.csv")

    logging.info("Creating activity_totals_table from {}".format(basename(acttot_src)))
    prepare_activity_totals(tr)

    tr.load_from_csv(
        "tech_opt.activity_totals",
        acttot_src
    )


def prepare_activity_totals(tr):
    tr.query("drop table if exists tech_opt.activity_totals cascade;")
    tr.query(
        "create table tech_opt.activity_totals\n"
        "(year_run integer,\n"
        "activity character varying,\n"
        "total_amount double precision);"
    )


def load_activity_totals_for_one_year(ps, tr, year):
    tr.query("truncate table tech_opt.activity_totals_temp;")

    tr.load_from_csv(
        "tech_opt.activity_totals_temp",
        join(ps.scendir, str(year), "ActivityTotalsI.csv")
    )

    tr.query(
        "insert into tech_opt.activity_totals\n"
        "select %(year)s, * from tech_opt.activity_totals_temp;",
        year=year
    )


def load_make_use(ps, tr, mu_year=None):
    if mu_year is None:
        path = join(ps.scendir, str(ps.baseyear))
    else:
        path = join(ps.scendir, str(mu_year))
    tr.query("set search_path = tech_opt;")
    tr.query("drop table if exists tech_opt.make_use cascade;")

    logging.info("Creating make use table")
    tr.query(
        "create table tech_opt.make_use\n"
        "(activity character varying,\n"
        "put_name character varying,\n"
        "moru character(1),\n"
        "coefficient double precision,\n"
        "stddev double precision,\n"
        "amount double precision);"
    )

    logging.info("Importing make use table")
    tr.load_from_csv(
        "tech_opt.make_use",
        join(path, "MakeUse.csv")
    )

    logging.info("Checking activity and put names using constraints")
    tr.query(
        "ALTER TABLE tech_opt.make_use\n"
        "ADD CONSTRAINT make_use_activity\n"
        "FOREIGN KEY (activity)\n"
        "REFERENCES tech_opt.activity(activity);"
    )

    tr.query(
        "ALTER TABLE tech_opt.make_use\n"
        "ADD CONSTRAINT make_use_put\n"
        "FOREIGN KEY (put_name)\n"
        "REFERENCES tech_opt.put(put_name);"
    )


def update_techopt(ps, tr, year, ref_year=None, output_file='TechnologyOptionsI.csv'):
    # TODO this seems slow. Although the query is now fast, the other part seems slow
    # Update this wekan card when fixed https://wekan.hbaspecto.com/b/y9u39s6FxLk6QmoyG/city-of-edmonton/QyJWCnkfP5RZuF5Jd
    logging.info(f"Writing {output_file} in {year}")
    result = tr.query(
        "select activity, option_name, option_weight, put_name_code, coefficient "
        "from tech_opt.coefficient_update where year_run = %(year)s", year=year)

    updates = dict_from_query(result)
    header, base = read_base_techopt(ps.scendir, ref_year)

    outfname = join(ps.scendir, str(year), output_file)
    #print("writing out "+str(outfname))
    with open(outfname, "w", newline="") as outf:
        writer = csv.writer(outf)
        writer.writerow(header)
        for row in base:
            act = row[0]
            opt = row[1]
            update_row = updates.get(act, {}).get(opt, {})
            new_row = list(row)
            for i, put in enumerate(header[2:], 2):
                if put in update_row:
                    new_row[i] = update_row[put]
            writer.writerow(new_row)

    if outfname != join(ps.scendir, str(year), "TechnologyOptionsScaled.csv"):
        logging.info(f"copying {outfname} to TechnologyOptionsScaled.csv")
        shutil.copy(
          outfname,
          join(ps.scendir, str(year), "TechnologyOptionsScaled.csv"),
        )
    if outfname != join(ps.scendir, str(year), "TechnologyOptionsI.csv"):
        logging.info(f"copying {outfname} to TechnologyOptionsI.csv")
        shutil.copy(
            outfname,
            join(ps.scendir, str(year), "TechnologyOptionsI.csv"),
        )
    # TODO perhaps this is slow
    # TODO this method (below) has hardcoded filenames so needs to be fixed!!
    # substitute_technology_columns(ps, year, ref_year)


def dict_from_query(result):
    tbl = {}
    for row in result:
        by_act = tbl.setdefault(row.activity, {})
        by_opt = by_act.setdefault(row.option_name, {})
        by_opt[row.put_name_code] = row.coefficient
    return tbl


def substitute_technology_columns(ps, year, ref_year=None):
    #TODO this may be slow https://wekan.hbaspecto.com/b/y9u39s6FxLk6QmoyG/city-of-edmonton/QyJWCnkfP5RZuF5Jd
    #TODO filenames are hardcoded
    header, options = read_techopt(join(ps.scendir, str(year), "TechnologyOptionsScaled.csv"))
    substitute_header, substitute = read_substitute(ps, year, ref_year)
    if substitute_header:
        substitute_indices = colindices(
            "TechnologyOptionsScaled in {}".format(year),
            header,
            *substitute_header
        )
        key_cols = _find_key_columns(
            "TechnologyOptionsScaled in {}".format(year), header
        )
        substitute_key_cols = _find_key_columns(
            "TechnologySubstitution in {}".format(year), substitute_header
        )
        substitute_dict = {tuple(elements(row, substitute_key_cols)): row for row in substitute}
        for option_row in options:
            key = tuple(elements(option_row, key_cols))
            if key in substitute_dict:
                for substitute_index, option_index in enumerate(substitute_indices):
                    option_row[option_index] = substitute_dict[key][substitute_index]

        write_techopt(join(ps.scendir, str(year), "TechnologyOptionsI.csv"), header, options)


def _find_key_columns(source, header):
    return colindices(source, header, "Activity", "OptionName")


def read_substitute(ps, year, ref_year):
    if ref_year is None:
        ref_year = ps.baseyear
    header, substitute = [], []
    for file_year in range(year, ref_year - 1, -1):
        header, substitute = _read_substitute_file(
            join(ps.scendir, str(file_year), "TechnologySubstitution.csv")
        )
        if header:
            break
    return header, substitute


def _read_substitute_file(path):
    try:
        return read_techopt(path)
    except IOError:
        return [], []


def read_base_techopt(scendir, ref_year):
    if ref_year is None:
        infname = join(scendir, "AllYears", "Inputs", "TechnologyOptionsI.csv")
    else:
        infname = join(scendir, str(ref_year), "TechnologyOptionsI.csv")
    return read_techopt(infname)


def read_techopt(path):
    with open(path, "r") as inf:
        reader = csv.reader(inf)
        header = next(reader)
        return header, list(reader)


def write_techopt(path, header, rows):
    with open(path, "w", newline="") as outf:
        writer = csv.writer(outf)
        writer.writerow(header)
        for row in rows:
            writer.writerow(row)


class Logger:
    # noinspection PyMethodMayBeStatic
    def log(self, text):
        logging.info(text)


def main():
    main_ps = pr.load_pecas_settings()
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--refyear',
        type=int,
        help="You can specify to use MakeUse.csv from a certain year, "
        "and TechnologyOptionsI.csv from that year as well. Otherwise "
        "TechnologyOptionsI comes from AllYears/Inputs, and MakeUse "
        "comes from the baseyear."
    )
    parser.add_argument(
        '--endyear',
        type=int,
        help="Will prepare technology scaling to this year (default = ps.end_year)."
    )
    parser.add_argument(
        '--firstyear',
        type=int,
        help="Will write out new TechnologyOptionsI.csv and ActivityTotalsI.csv to this year."
    )

    # harley says it will automatically change the dash to an underscore for us
    parser.add_argument(
        '--output-file', '--outputfile',
        help="The name of the TechnologyOptions file to write. "
        "(use 'TechnologyOptionsScaled' if you using OptionChanges "
        "to further modify the AA inputs)")

    args = parser.parse_args()

    pr.set_up_logging(main_ps)

    output_file = args.output_file

    if output_file is None:
        print("=== require '--output-file' option.")
        return -1

    if not output_file.endswith(".csv"):
        output_file = output_file + ".csv"
        print("Output file is "+str(output_file))

    create_all_acttot_techopt(
        main_ps,
        lambda: pr.connect_to_sd(main_ps),
        first_year=args.firstyear,
        ref_year=args.refyear,
        end_year=args.endyear,
        output_file=output_file
    )


if __name__ == "__main__":
    main()
