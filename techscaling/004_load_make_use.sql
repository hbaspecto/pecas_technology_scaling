
set search_path = tech_opt;
drop table if exists tech_opt.make_use cascade;


create table tech_opt.make_use
        (activity character varying,
        put_name character varying,
        moru character(1),
        coefficient double precision,
        stddev double precision,
        amount double precision);

--    tr.load_from_csv(
--        "tech_opt.make_use",
 --       join(path, "MakeUse.csv")
--    )


        ALTER TABLE tech_opt.make_use
        ADD CONSTRAINT make_use_activity
        FOREIGN KEY (activity)
        REFERENCES tech_opt.activity(activity);

        ALTER TABLE tech_opt.make_use
        ADD CONSTRAINT make_use_put
        FOREIGN KEY (put_name)
        REFERENCES tech_opt.put(put_name);
