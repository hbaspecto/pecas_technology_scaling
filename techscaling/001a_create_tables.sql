set search_path = tech_opt;
drop table if exists tech_opt.technology_options cascade;

        create table tech_opt.technology_options
        (activity character varying,
        option_name character varying,
        option_weight double precision,
        put_name_code character varying,
        coefficient double precision);


--    tr.load_from_csv(
--        "tech_opt.technology_options",
--        join(path, "TechOptLongThin.csv"),
--        header=False
--    )