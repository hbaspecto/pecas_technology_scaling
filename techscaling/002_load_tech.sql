alter table tech_opt.technology_options add column put_name character varying, add column moru character(1);
update tech_opt.technology_options
    set moru = case when position(':' in put_name_code) >0 then 'U' else 'M' end,
    put_name = substring(put_name_code from 0 for case when position(':' in put_name_code) > 0 then position(':' in put_name_code) else char_length(put_name_code)+1 end);

select null as "Creating activities table";
drop table if exists tech_opt.activity cascade;
create table tech_opt.activity as (select activity from tech_opt.technology_options group by activity order by activity);
alter table tech_opt.activity add column activity_type character(1);
update tech_opt.activity set activity_type='E' where activity like %(exporter)s;
update tech_opt.activity set activity_type='I' where activity like %(importer)s;


select null as "Creating options table";
drop table if exists tech_opt.tech_option cascade;
create table tech_opt.tech_option as (select activity, option_name, avg(option_weight) as weight from tech_opt.technology_options group by activity, option_name order by activity, option_name);

select null as "Creating puts table";
drop table if exists tech_opt.put cascade;
create table tech_opt.put as (select put_name from tech_opt.technology_options group by put_name order by put_name);

select null as "removing zeroes from technology options";
delete from tech_opt.technology_options where coefficient = 0;

alter table tech_opt.activity add unique (activity);

ALTER TABLE tech_opt.technology_options 
   ADD CONSTRAINT tech_opt_activity
   FOREIGN KEY (activity) 
   REFERENCES tech_opt.activity(activity);
   
alter table tech_opt.put add unique (put_name);

ALTER TABLE tech_opt.technology_options 
   ADD CONSTRAINT tech_opt_put
   FOREIGN KEY (put_name) 
   REFERENCES tech_opt.put(put_name);
