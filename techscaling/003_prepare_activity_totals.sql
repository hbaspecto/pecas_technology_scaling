drop table if exists tech_opt.activity_totals cascade;
create table tech_opt.activity_totals
        (year_run integer,
        activity character varying,
        total_amount double precision);


--    tr.load_from_csv(
--        "tech_opt.activity_totals",
--        acttot_src