set search_path = tech_opt;

drop view if exists tech_opt.make_future cascade;
create view tech_opt.make_future
as 
select year_run, put_name, sum(make_use_base_amount) as old_amount, sum(makeuse_amount) as new_amount, 
sum(makeuse_amount)/sum(make_use_base_amount) as scale_factor
from tech_opt.all_4_amounts
where moru = 'M' and 
activity not like %(importer)s
group by year_run, put_name;

-- Scale exporters initially in proportion to make amount changes (make factor)

update tech_opt.activity_totals u
set total_amount = x.scaled_amount from
(
select mf.year_run, a.activity, a.total_amount as base_amount, 
mf.scale_factor, a.total_amount * mf.scale_factor as scaled_amount
from tech_opt.activity_totals a
join tech_opt.aef_make_use mu
	on a.activity = mu.activity
and a.activity like %(exporter)s
join tech_opt.make_future mf
	on mf.put_name = mu.put_name
	and mu.year_run = mf.year_run
where a.year_run = %(baseyear)s
) x
where u.year_run = x.year_run
and u.activity = x.activity
and u.year_run != %(baseyear)s;

