#!/bin/bash

# Windows line problems
echo "Converting line endings"
perl -pi -e 's/\r\n?/\n/g' "$TECHDIR/TechnologyOptionsI.csv"
perl -pi -e 's/\r\n?/\n/g' "$ACTDIR/All_ActivityTotalsI.csv"
perl -pi -e 's/\r\n?/\n/g' "$MUDIR/MakeUse.csv"

echo "Uncrosstabbing TechnologyOptionsI.csv"
awk -F "," -v OFS="," 'NR==1{
for(i=2;i<=NF;i++)
head[i]=$i
}
NR>1{
for(i=4;i<=NF;i++)
print $1,$2,$3,head[i],$i
}' $TECHDIR/TechnologyOptionsI.csv > $TECHDIR/TechOptLongThin.csv
