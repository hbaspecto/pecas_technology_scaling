# Machine-specific settings for PECAS.
# On each machine, create a copy of this template called machine_settings.py, then adjust the settings as needed for that machine.
# The machine_settings.py file is included in svnignore, so it will not be committed to the repository. This is by design. DO NOT change it!

postgres = "postgres"
sqlserver = "sqlserver"

# Using postgreSQL or SQL Server for SD?
sql_system = postgres
#sql_system = postgres

# Define databasename and user in postgresSQL
pguser="postgres"
pgpassword="postgres"      
# password can sometimes also be in .pgpass file in users home directory
# WARNING the default installation template for MAPIT does not have enough permissions for the usrPostgres user.  If usrPostgres is not a
# 'superuser' in the database you will have to change the ownership of the tables in the mapit schema to usrPostgres (using right-click in pgAdminIII
# or the database command 'ALTER TABLE output.loaded_scenarios OWNER TO "usrPostgres"')   

# PECAS run configuration
scendir = "./" #"./" means current directory, i.e. usually the same directory where this file is located 
sd_host="lakelouise.office.hbaspecto.com"
sd_database="alberta_db"
sd_user="usrPostgres"
sd_password="usrPostgres"
sd_port=5432

mapit_host="oyen.office.hbaspecto.com"
mapit_port=5432
mapit_schema="output"
mapit_database='run_log'

# installation configuration
pgpath="/opt/local/bin/"
javaRunCommand="/usr/bin/java"
