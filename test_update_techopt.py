import psycopg2 as pg
import os
import pecas_routines as pr
import subprocess

from techscaling import update_techopt

def connect():
    return pg.connect(
            database=os.environ['PGDATABASE'],
            host=os.environ['PGHOST'],
            port=5432,
            user=os.environ['PGUSER'],
            password=os.environ['PGPASSWORD'])

scendir = r"./"

ps = pr.load_pecas_settings()
pgcmd = os.path.join(ps.pgpath, "psql")
for year in range(ps.baseyear+1, ps.endyear+1):
    actot = os.path.join(scendir, "AllYears", "Inputs", "All_ActivityTotalsI.csv")
    update_techopt.create_all_acttot_techopt(ps, connect, actot, year)
    sqlstr = (
        "\\copy (select activity as \"Activity\", total_amount as \"TotalAmount\" from tech_opt.activity_totals "
        "where year_run=" + str(year) +
         ") to '" + os.path.join(ps.scendir, str(year), "ActivityTotalsI.csv") + "' csv header"
    )
    #retcode = subprocess.check_call([pgcmd, "-c",sqlstr, "--host="+ps.mapit_host, "--port="+str(ps.mapit_port) , "--dbname="+ps.mapit_database, "--username="+ps.pguser])
