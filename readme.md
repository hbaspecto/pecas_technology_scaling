HTML header:   <script type="text/javascript"
    src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
    </script>

Author: John Abraham

Date: March 12, 2020

Title: PECAS Adaptive Economy Size and Technology Scaling

# PECAS Adaptive Economy Size ED Module

## License

This software is used in many [PECAS](https://www.hbaspecto.com/products/pecas)
installations, and is essential to running PECAS. It is provided
under the [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0) since it is now an essential part of
PECAS which is provided under the same license.  See the NOTICE.txt file.

## Explanation



## Software Implementation

Setup:

- User specifies a base scenario by code (e.g. 'I174') that has already
run at least a few years ahead, and has a different scenario name than
the current scenario

- Set a coefficient for the growth-per-consumer-surplus-change for
activities in a csv input table called
AllYears/Inputs/GrowthPerBenefit.csv, note not all activities need to
have a growth elasticity, not present assumes zero (no additional
growth)

- Scenarios need to have identical conditions including an identical set
of activities, and AllYears/Inputs/AllActivityTotalsI.csv should also
have the same values.

1) If aa is to be run for the year, use an SQL Query to get
ExchangeResults from a base scenario in the mapit database for the year,
and write it out to ExchangeResultsI.csv. This is for any years that AA
is running, even the base year.

- If the base scenario results aren't there, error and crash. (Potential enhancement-- offer to wait for the base scenario to get ahead again.)

2) If AA is to be run for the year, Run AA for year.

3) load Mapit Series 1 (the base AA results) to MapIt (occurs as part of
running AA, but if AA is not running this year then we already load the
output files that have been copied into this directory from the previous
year.)

3) If AA is to be run the following year, query
`output.consumer_surplus` to determine the `consumersurpluschange` by
activity between the base scenario and the current scenario, multiply
the consumer surplus change by the coefficient specified for the
activity (if any) from `GrowthPerBenefit.csv` to determine the
additional amount of activity (could be negative) to be added to the
amount specified for the following year in AllActivityTotalsI.csv. Write
the ActivityTotalsI.csv file for the following year.

4) Redo the technology scaling for the following year.  Be sure to use the new ActivityTotalsI for the next year, and not the original from AllActivityTotals that has been loaded in the tech_opt database.

( Prototype spreadsheet is `/ASET/ASET Working/1.0 Model Development/Area 1.1. SEM/Task 1.1.3. ED/MRA213.Initial Aggregate Economic Model/IndustryGrowthElasticity/Prototype from I159b and P161b.xlsx`)

## Additional checks and features

1) If replaying the run, don't do this for replayed AA years.  (We
shouldn't be regenerating ActivityTotalsI files nor
TechnologyOptionsI.csv for years that are being replayed, and
ExchangeResultsI also already exists.)

2) Since we are writing TechnologyOptionsI.csv for each year based on
the previous year, the TechnologyScaling script does not need to write
out each year's TechnologyOptionsI.csv in advance -- they'll just get
blown away anyways.


# PECAS Technology Scaling

## Overview and Purpose

A [PECAS](http://www.hbaspecto.com/pecas) model is a spatial economic
model of a region, used to forecast future spatial patterns and economic
performance, and to analyze policy influence on spatial patterns and
economic performance.  It consists of two main modules, the Activity
Allocation Module (AA) and Space Development module (SD), and interacts
with a travel demand model (TM) which forecasts the future performance
of the travel infrastructure and services.

The AA module also interacts with an Economic and Demographic model,
called ED.  The goal of ED is to represent the overall performance and
structure of the economy. The [PECAS Overview Figure][pecas1] shows the
general flow of data in a PECAS implementation and the role of the ED
model. The ED model is responsible for taking, as input, measures of the
overall situation in the region, and producing the quantity amount of
each activity in the region, as well as the base level of technology in
the region.

![Pecas Overview with Technology Adjustment][pecas1]

[pecas1]: docs/technology_scaling_overview.png "Overall structure of a PECAS model with technology scaling over time."

The AA module represents decision makers called "Activities",
interacting with each other through their "Puts".  The word "Puts" is a
abstraction from the words "Input" and "Output", since the ways in which
Activities interact with each other are represented as the financial
relationships in an [Input/Output
model](https://en.wikipedia.org/wiki/Input–output_model). Activities are
represented as categories of households and industry, along with some
other institutions such as government agencies.  As money flows in one
direction from purchaser to supplier, the "Put" flows as an output from
the supplier and as an input to the consumer. For example, labour is an
output of households, and commuting patterns on the transportation
network are the delivery mechanism for delivering labour to business
establishments in various industries, who require labour as an input to
their production process. Categories of Puts are commonly restricted to
several types: Goods, Services, Labour, Space (buildings and other
fixed-in-place assets), and Financial (nontangible relationships not
requiring travel for delivery.)

The relationships need to be balanced.  For every category of Put:

\\[ made + imported + netexogenous = used + exported \\]

or


\\[ M_p^y + I_p^y + ne_p^y = U_p^y + E_p^y \\]

with superscript \\(y\\) representing a future year, and subscript \\(p\\) representing
a Put category. ( \\(y\\) and \\(p\\) will be omitted when not necessary in the discussion below)

## Mechanism

A "technical coefficient" is a numerical measure of the amount of Put made (or used) per
unit of activity. In general, Activities are given "Technology Options" which allow them to choose
different rates of production or consumption.  We have technical coefficients \\( \alpha_p \\) associated
with each option, as well as the average of the realized technical coefficients from the base year \\(b\\):

\\[ \overline \alpha_{a,p}^b  = \frac {Q_{a,p}^b}{S_a^b} \\]

where \\( Q_{a,p}^b \\) is the amount of Put \\(p\\) made (or used, represented as negative amounts)
by activity \\(a\\) in the base
year \\(b\\), and \\({S_a^b}\\) is the size of Activity \\(a\\) in the base year \\(b\\).

As the amount of each activity changes in future
forecast years, it can throw off the balance in AA.  For example, if the total quantity
of industry grows in a region grows faster than the number of employable people,
and once unemployment has been
eliminated, there are only two possibilities:

1) More net people commute across the border of the region (either an increase in the
import of labour, a decrease in the export of labour, or both.)

2) Industry (businesses) require less labour per unit of production, that is their technology
has changed and labour productivity has increased.

The PECAS Technology Scaling feature is used to adjust technical coefficients and import/export
capacity in future
years so that AA can represent a balanced economy. It takes the user specified TechnologyOptions table
describing all of the available technical coefficients in the different technology options \\( \alpha_{a,p} \\),
and it also takes an AA generated MakeUse table, which contains
the realized average technical coefficients
\\( \overline \alpha_{a,p}^b \\) from an equilibrium representation from the base year.

First, the total expected future year \\( y \\) make \\( made \\) is calculated for each put as:

\\[ M_p^y = \sum_a {{S_a^y} \cdot \overline \alpha_{a,p}^b}, \forall \overline \alpha_{a,p}^b > 0 \\]

The AA module has "export consumers" for each Put, representing various entities outside
of the model region who are willing to buy the production from the model region.  The second
step is to scale the quantity of these export consumers up by the change in \\(M_p\\), with
the idea that for most Put categories, the region is a price taker on exports and there is enough
parallel growth in potential consumption capacity in the rest of the world to consume the
increased production of the model region.

Thus all else being equal, we expect the amount of exports to scale with the size of the export
consumers for one year \\(y\\) as compared to the base year \\(b\\):

\\[E_p^{y*} = E_p^b \cdot \frac {M_p^y}{M_p^b} \\]

The AA module also has "import producers" for each Put, representing various entities outside
of the model region who are willing to sell production to the model region.  The third
step is to scale the quantity of these import producers up by the change in \\(U_p\\), with
the idea that for most Put categories, the region is a price taker on imports and there is enough
growth in potential production capacity in the rest of the world to supply the increased
needs of the model region.

\\[ U_p = - \sum_a {{S_a^y} \cdot \overline \alpha_{a,p}^b}, \forall \overline \alpha_{a,p}^b < 0\\]

Thus all else being equal, we expect the amount of imports to scale with the size of the import
providers for one year \\(y\\) as compared to the base year \\(b\\):

\\[I_p^{y*} = I_p^b \cdot \frac {U_p^y}{U_p^b} \\]

We assume the net exogenous supply and demand \\( ne_p^y \\) is constant, that is \\(ne_p^y = ne_p^b \\).
(As a model is constructed over
iterations of model development, the exogenous supply and exogenous command should be reduced
to a small amount, as we want to explicitly represent the supply and demand of Puts
by Activities, Export Consumers, and Import Providers.  So, in final production PECAS
simulation models this element is near to zero and insignificant.)

Without any further adjustments, we would expect the amount of use of each Put by
each activity to scale with their size:

\\[ U_p^{y*} = \sum_a {{S_a^y} \cdot \overline \alpha_{a,p}^b} , \forall \overline \alpha_{a,p}^b <0 \\]

To balance each put, we calculate an adjustment factor \\(f_p^y\\) for each year, solving the
equation:

\\[  M_p^y + (1+f_p^y) \cdot I_p^{y*} + ne_p^b = (1-f_p^y) \cdot ( U_p^{y*} + E_p^{y*} )\\]

for \\(f_p^y\\):

\\[ f_p^y = \frac {U_p^{y*} + E_p^{y*}  - M_p^y  - I_p^{y*} - ne_p^b }{I_p^{y*} +  U_p^{y*} + E_p^{y*} } \\]

We now adjust the use coefficients for each option before running AA in a future year:

\\[ \alpha_{a,p}^y =  \alpha_{a,p}^b \cdot (1 - f_p^y) \forall  \alpha_{a,p}^b < 0 \\],

and we further adjust the Import Provider and Export Consumer quantities for AA in a future year by applying the \\(f_p^y\\) factor, so that our
a priori expectation before AA runs is:

\\[I_p^y = I_p^{y*} \cdot (1 + f_p^y) \\]

and

\\[E_p^y = E_p^{y*} \cdot (1 - f_p^y) \\]

## Summary of mechanism and future enhancements

A number of simplified assumptions are made in this implementation.  These assumptions are
detailed below, along with a suggestion of how they may be relaxed in future, more sophisticated, versions
of this module.

1) _Make coefficients are not scaled_.  This seems especially appropriate for for industry, since industry
is measured in terms of its production quantity.  However, for households the quantity of
labour production is elastic in AA through Technology Choice, so it may not be necessary to scale
the coefficients associated with each of the technology options that make up the set of available
technologies available in the technology choice.

2) _Activity amounts are not scaled_.  This respects the forecast provided by third
parties responsible for forecasts, and may be particularly appropriate
for "basic" industries which are export driven and are driving the economy.
For secondary industries serving internal demand, we may wish to adjust the size of the
industry in line with the way the PECAS model forecasts changes in internal demand quantities.

3) _The "rest of the world"_, represented by import providers and export consumers, _grows
at the same rate as the Put production or consumption in the region_. For certain commodities
with high (relative) transportation costs, or for example in Alberta which is a dominant player in the
world economy (e.g. oil and energy), this may not be an appropriate or accurate assumption.

4) For the final balance, the _"rest of the world" and internal use coefficients are scaled proportionally_.
As an enhancement, each Put category could be investigated specifically, and the user could
specify, for each put, that this balancing factor should be applied more heavily to
imports, exports or internal use.  Further, certain Puts are already elastic in consumption
in AA's technology choice module,
so scale based on availability, and it may not be necessary to scale the individual coefficients for each
of the technology options that make up the set of available technologies in the technology choice.



## Software Implementation

The technology scaling implementation of the Economic Demographic
model works using a series of SQL scripts and some command line
scripts.  These are stored in the [bitbucket repository called
pecas_technology_scaling](https://bitbucket.org/hbaspecto/pecas_technology_scaling).
The scripts can be run using the [hba-parts](https://bitbucket.org/hbaspecto/hba-parts)
infrastructure, which manages dependencies between them.

The individual parts are documented with in-line documentation, and
their dependencies are are currently described in the files
themselves, but this graph dependency is being moved to a separate
.yaml file in the next version of hba-parts, so please check the
current repository contents to review the current parts and their
dependencies.

The following table describes the current parts:

| Name      				  |    Purpose
|-------     				  |----------------
| `001_prepare_for_import.sh` | Convert line endings, uncrosstab files
| `002_load_tech.sql`         | Setup technology options in database
| `003_load_activity_totals.sql` | Load forecast economic and demographic totals for all years
| `004_load_makeuse.sql`      | Load MakeUse.csv table from base year model run
| `005_check_names.sql`       | Check input consistency cross references
| `010_aef_view.sql`          | Create view with four different AEF tables
| `012_aef_crosstab.sql`      | Generate syntax for AEF table in crosstab format
| `020_scale_exporters.sql`   | Scales exporters based on total internal make (all make can be exported)
| `021_scale_importers.sql`   | Scales importers and calculates use coefficient scaling
| `022_calculate_use_scale.sql` | Creates a view with the scaled use coefficients

### Running pecas_technology_scaling alone

#### Installing hba-parts
```
# First, make sure hba-parts is installed and active
# user based install like this doesn't work yet
# pip3 install 'git+ssh://git@bitbucket.org/hbaspecto/hba-parts.git#egg=hba_parts'

# but developer based install works
git clone 'http://john_abraham_hba@bitbucket.org/hbaspecto/hba-parts.git'
cd hba-parts
source hba-setup.env
make _ve_rebuild
```
#### Running pecas_technology_scaling

```
cd
git clone 'https://john_abraham_hba@bitbucket.org/hbaspecto/pecas_technology_scaling.git'
cd pecas_technology_scaling
source scripts/tech_opt.env
hba-parts scripts --run --force
```
### Running pecas_technology_scaling within an integrated Spatial Economic Model run

In `pecas.yml` set the following option:

	scale_technology_options: true

This will tell the main run script `runpecas.py` to call the functions in the file `update_techopt.py`.
These functions interact with the database in the input schema.
